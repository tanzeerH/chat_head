package chathead.apparnest.com.chathead;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startService(new Intent(MainActivity.this,ChatHead.class));
        Toast.makeText(MainActivity.this,"Floating Head Started",Toast.LENGTH_SHORT).show();
        finish();
    }
}
