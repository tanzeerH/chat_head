package chathead.apparnest.com.chathead;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Acer on 5/30/2016.
 */
public class SearchActivity extends Activity{



    private SearchView sv;
    private TextView tv;
    public BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("MSG>>>", "on receive");
            // unregisterReceiver(this);
            finish();
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        sv=(SearchView)findViewById(R.id.sv);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Toast.makeText(SearchActivity.this,query,Toast.LENGTH_SHORT).show();
                if(hasInternet(SearchActivity.this))
                    new GetWordMeaning(query).execute();
                else
                  alert(SearchActivity.this,"Please check internet connection.");
                return  false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        sv.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
               finish();
                return  false;
            }
        });
        sv.setIconified(false);

        tv=(TextView)findViewById(R.id.tv_res);

        Intent i=getIntent();
        if(i!=null)
        {
            String word=i.getStringExtra("word");
            if(word!=null && word.length()!=0)
            {
                if(hasInternet(SearchActivity.this))
                    new GetWordMeaning(word).execute();
                else
                    alert(SearchActivity.this,"Please check internet connection.");
            }
        }


    }
    private class  GetWordMeaning extends AsyncTask<Void,Void,Void>
    {

        String urlString="http://wordnetweb.princeton.edu/perl/webwn?s=";
        String responseString="";
        String word="";
        public GetWordMeaning(String w) {
           this.word=w;
        }

        @Override
        protected Void doInBackground(Void... params) {

           /* URL url;
            HttpURLConnection connection = null;
            try {
       // Create connection
                url = new URL(urlString+word);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
// Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());

                wr.flush();
                wr.close();
// Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                String responseStr = response.toString();
                Log.e("Server response",responseStr);
                responseString=responseStr;
            } catch (Exception e) {

                e.printStackTrace();

            } finally {

                if (connection != null) {
                    connection.disconnect();
                }
            }*/
            try {
                Document doc = Jsoup.connect(urlString+word).get();
                Elements header= doc.getElementsByTag("h3");

                String h3="";
                if(header!=null && header.size()>0)
                {
                    h3=header.get(0).toString();

                }
               Elements uList= doc.getElementsByTag("ul");
                ArrayList<Element> elemList=new ArrayList<>();
               if(uList!=null && uList.size()>0)
               {
                   Element ul=uList.get(0);

                   for( Element e: ul.children())
                   {
                       if(responseString.length()==0)
                           responseString+=e.toString();
                       else
                          responseString+="<br>"+e.toString();
                   }
               }
                responseString=h3+"<br>"+responseString;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tv.setVisibility(View.VISIBLE);
            tv.setText(Html.fromHtml(responseString));


        }
    }
    public static boolean hasInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static void alert(Activity context, String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(context);
        bld.setTitle("Float Search");
        bld.setMessage(message);
        bld.setCancelable(false);
        bld.setNeutralButton("Ok", null);
        bld.create().show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter("activity_finish"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
