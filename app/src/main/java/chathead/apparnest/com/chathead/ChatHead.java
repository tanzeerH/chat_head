package chathead.apparnest.com.chathead;

import android.app.Dialog;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

/**
 * Created by Acer on 5/30/2016.
 */
public class ChatHead extends Service {

    private WindowManager windowManager;
    private ImageView chatHead;
    private ImageView chatDelete;
    private WindowManager.LayoutParams params;
    private WindowManager.LayoutParams paramssearch;
    private WindowManager.LayoutParams paramsDelete;
    private SearchView searchView;
    private boolean isSearchViewOpened=false;
    private int width=100;
    private int height=100;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    ClipboardManager.OnPrimaryClipChangedListener mPrimaryChangeListener = new ClipboardManager.OnPrimaryClipChangedListener() {
        public void onPrimaryClipChanged() {

            //Toast.makeText(getApplicationContext(),"query",Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(getApplicationContext(),SearchActivity.class);
            ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
            CharSequence pasteData="";
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            pasteData = item.getText();
            intent.putExtra("word",pasteData);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    };
    @Override
    public void onCreate() {
        super.onCreate();


        Log.e("service","created");
        showChatHead();
        setWidthHeight();
        //showDelete();
        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);

        windowManager.addView(chatHead, params);

    }

    private void showDeleteButoon(int x, int y)
    {
        if(y>height-300)
        {


        }
    }
    private void showChatHead()
    {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        chatHead = new ImageView(this);
        chatHead.setImageResource(R.drawable.chat_head);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        showDelete();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if(shouldDelete()) {
                            sendBroadcast();
                            stopSelf();

                        }
                        else {
                            if (isClicked(initialX, initialY)) {
                                if (isSearchViewOpened) {

                                    isSearchViewOpened = false;
                                    sendBroadcast();
                                } else {
                                    isSearchViewOpened = true;
                                    chatHead.performLongClick();
                                }

                            }
                            removeDelete();
                        }


                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHead, params);
                       // Log.e("co ordinate","x: "+params.x+" y  "+ params.y );
                        return true;
                }
                return false;
            }
        });

        chatHead.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                Intent intent=new Intent(getApplicationContext(),SearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return false;
            }
        });
    }
    private void sendBroadcast() {
        Log.e("MSG", "sending broadcast");
        Intent intent = new Intent();
        intent.setAction("activity_finish");
        sendBroadcast(intent);
    }
    private void closeSearchView()
    {
        if (searchView != null)
            windowManager.removeView(searchView);
    }
    private void showSearchView()
    {
        searchView=new SearchView(this);
        paramssearch = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        paramssearch.gravity = Gravity.TOP | Gravity.LEFT;
        paramssearch.x = 0;
        paramssearch.y = 100;

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(getApplicationContext(),"query "+ query,Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.requestFocus();
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {


                Log.e("sv focused",""+hasFocus);
                    showInputMethod(view.findFocus());

            }
        });
       windowManager.addView(searchView,paramssearch);

    }
    private void showDialog()
    {
        paramssearch = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        paramssearch.gravity = Gravity.TOP | Gravity.LEFT;
        paramssearch.x = 0;
        paramssearch.y = 100;
        Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.activity_search);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
       windowManager.addView(dialog.getWindow().peekDecorView(),paramssearch);
    }
    private void showInputMethod(View view) {
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).
                toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
    private boolean isClicked(int x,int y)
    {
        if(Math.sqrt((params.x-x)*(params.x-x)+ (params.y-y)*(params.y-y))<50)

            return  true;
        else
            return  false;
    }
    private void showDelete()
    {
        chatDelete  = new ImageView(this);
        chatDelete.setImageResource(R.drawable.delete);

        paramsDelete = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        paramsDelete.gravity = Gravity.TOP | Gravity.LEFT;
        paramsDelete.x = width/2;
        paramsDelete.y = height-100;

        windowManager.addView(chatDelete, paramsDelete);

    }
    private void removeDelete()
    {
        if (chatDelete != null)
            windowManager.removeView(chatDelete);

    }
    private boolean shouldDelete()
    {

       // Log.e("params", params.x+"  "+ params.y+"  "+paramsDelete.x+"  "+paramsDelete.y);
       // Log.e("ditance: ",""+Math.sqrt((params.x-paramsDelete.x)*(params.x-paramsDelete.x)+ (params.y-paramsDelete.y)*(params.y-paramsDelete.y)));
        if(Math.sqrt((params.x-paramsDelete.x)*(params.x-paramsDelete.x)+ (params.y-paramsDelete.y)*(params.y-paramsDelete.y))<400)

            return  true;
        else
            return  false;
    }
    private void setWidthHeight()
    {
        width= getApplicationContext().getResources().getDisplayMetrics().widthPixels;
        height= getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null)
            windowManager.removeView(chatHead);
        if (chatDelete != null)
            windowManager.removeView(chatDelete);
      }
}
